// both/collections/posts.js

Posts = new Mongo.Collection("posts");
Posts.attachSchema(new SimpleSchema({
  MAP_Ref: {
        type: String,
        label: "MAP Ref Num",
        allowedValues: ['1', '2', '3'],
        autoform: {
            options: [
                { label: "1", value: "1" },
                { label: "2", value: "2" },
                { label: "3", value: "3" }
            ]
        }
    },
    Block: {
        type: String,
        label: "Block",
        allowedValues: ['A', 'B', 'C'],
        autoform: {
            options: [
                { label: "A", value: "A" },
                { label: "B", value: "B" },
                { label: "C", value: "C" }
            ]
        }
    },
    Room_num: {
        type: String,
        label: "Room Number"
    },
    Location_desc: {
        type: String,
        label: "Location-desc",
        allowedValues: ['in-room', 'outside'],
        autoform: {
            options: [
                { label: "in-room", value: "in-room" },
                { label: "outside", value: "outside" },
            ]
        }
    },
    AP_name: {
        type: String,
        label: "AP Name"
    },
    ConnectedBSSID: {
        type: String,
        label: "Connected BSSID"
    },
    Date: {
        type: String,
        label: "Date"
    },
    Time: {
        type: String,
        label: "Time"
    },
    Device: {
        type: String,
        label: "Device",
        allowedValues: ['A', 'B', 'C'],
        autoform: {
            options: [
                { label: "A", value: "A" },
                { label: "B", value: "B" },
                { label: "C", value: "C" },
            ]
        }
    },
    Download_speed: {
        type: Number,
        label: "Download (Mbps)"
    },
    Upload_speed: {
        type: Number,
        label: "Upload (Mbps)"
    },
    Remark: {
        type: String,
        label: "Remark"
    },
    
}));